### O (Objective): What did we learn today? What activities did you do? What scenes have impressed you?

​	上午进行了Code Review，Daniel指出我的代码有多余操作，不够简洁。然后学习了如何对Spring Boot进行集成测试，学习了Controller，Service，DAO三层架构，并将之前的Controller,Repository完成的案例重构成三层架构，最后学习了对Spring Boot进行单元测试，修改Expection Code。

### R (Reflective): Please use one word to express your feelings about today's class.

​	内容很多

### I (Interpretive): What do you think about this? What was the most meaningful aspect of this activity?

​	我能感觉到今天学习的内容对今后的实际工作开发非常有用，能够大大降低测试与修改的成本，今天的敲代码环节特别多，我使用了Stream流对项目进行编写，感觉到自己对Stream流与Spring Boot框架的理解与运用有了一定提升，不会的地方也会上网查找解决问题。

### D (Decisional): Where do you most want to apply what you have learned today? What changes will you make?

​	今天重点学习并编码了Spring Boot测试相关的内容，感觉大量的边写代码对自己能力的提升非常有用。接下来会多多敲代码，对身边的人的代码优点多多学习。

package com.afs.restapi.service;

import com.afs.restapi.controller.EmployeeController;
import com.afs.restapi.exception.AgeNotMatchSalaryException;
import com.afs.restapi.exception.AgeOutOfRangeException;
import com.afs.restapi.exception.NotFoundException;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.EmployeeRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class EmployeeService {
    private final EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public List<Employee> getEmployeeList() {
        return employeeRepository.findAll();
    }

    public Employee findById(Long id) {
        if (employeeRepository.findById(id).isStatus()) {
            return employeeRepository.findById(id);
        }
        throw new NotFoundException();
    }

    public List<Employee> findByGender(String gender) {
        List<Employee> employeeList = employeeRepository.findByGender(gender);
        return employeeList.stream().filter(Employee::isStatus).collect(Collectors.toList());
    }

    public List<Employee> findByPage(int pageNumber, int pageSize) {
        List<Employee> employeeList = employeeRepository.findByPage(pageNumber,pageSize);
        return employeeList.stream().filter(Employee::isStatus).collect(Collectors.toList());
    }

    public Employee insert(Employee employee) {
        if (employee.getAge() < 18 || employee.getAge() > 65) {
            throw new AgeOutOfRangeException();
        }
        if (employee.getAge() > 30 && employee.getSalary() < 20000) {
            throw new AgeNotMatchSalaryException();
        }
        employee.setStatus(true);
        return employeeRepository.insert(employee);
    }

    public Employee update(Long id, Employee employee) {
        if (employeeRepository.findById(id).isStatus()) {
            return employeeRepository.update(id, employee);
        }
        throw new NotFoundException();
    }

    public void delete(Long id) {
//        employeeRepository.delete(id);
        employeeRepository.findById(id).setStatus(false);
    }
}
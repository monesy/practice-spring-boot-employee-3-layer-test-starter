package com.afs.restapi;


import com.afs.restapi.exception.NotFoundException;
import com.afs.restapi.model.Company;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.CompanyRepository;
import com.afs.restapi.repository.EmployeeRepository;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class CompanyControllerTest {
    @Autowired
    MockMvc mockMvc;

    ObjectMapper mapper = new ObjectMapper();

    @Autowired
    CompanyRepository companyRepository;

    @BeforeEach
    void setUp() {
        companyRepository.clearAll();
    }
    private static Company buildCompany1() {
        Company company1 = new Company(1L, "company1");
        return company1;
    }
    private static Company buildCompany2() {
        Company company2 = new Company(2L, "company2");
        return company2;
    }
    private static Company buildCompany3() {
        Company company3 = new Company(3L, "company3");
        return company3;
    }

    @Test
    void should_get_all_companies_when_perform_get_given_companies() throws Exception {
        Company company = buildCompany1();
        companyRepository.addCompany(company);

        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/companies"))
                .andReturn();

        MockHttpServletResponse response = mvcResult.getResponse();
        assertEquals(200, response.getStatus());
        List<Company> companies = mapper.readValue(response.getContentAsString(), new TypeReference<List<Company>>() {
        });
        assertEquals(1, companies.size());
        Company companyGet = companies.get(0);
        assertEquals(1, companyGet.getId());
        assertEquals("company1", companyGet.getName());
    }

    @Test
    void should_get_all_companies_when_perform_get_given_companies_use_matchers() throws Exception {
        //given
        Company newCompany = buildCompany1();
        companyRepository.addCompany(newCompany);

        //when
        mockMvc.perform(MockMvcRequestBuilders.get("/companies"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").isNumber())
                .andExpect(jsonPath("$[0].id").value(1))
                .andExpect(jsonPath("$[0].name").isString())
                .andExpect(jsonPath("$[0].name").value("company1"));
    }

    @Test
    void should_return_company_company1_with_id_1_when_perform_get_by_id_given_companies_in_repo() throws Exception {
        //given
        Company company1 = buildCompany1();
        Company company2 = buildCompany2();
        companyRepository.addCompany(company1);
        companyRepository.addCompany(company2);


        //when
        mockMvc.perform(MockMvcRequestBuilders.get("/companies/1"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.name").isString())
                .andExpect(jsonPath("$.name").value("company1"));
    }

    @Test
    void should_return_one_page_companies_when_perform_get_by_page_given_companies_in_repo() throws Exception {

        //given
        Company company1 = buildCompany1();
        Company company2 = buildCompany2();
        Company company3 = buildCompany3();

        companyRepository.addCompany(company1);
        companyRepository.addCompany(company2);
        companyRepository.addCompany(company3);


        //when
        mockMvc.perform(MockMvcRequestBuilders.get("/companies?page=1&size=2"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id").isNumber())
                .andExpect(jsonPath("$[0].id").value(1))
                .andExpect(jsonPath("$[0].name").isString())
                .andExpect(jsonPath("$[0].name").value("company1"))

                .andExpect(jsonPath("$[1].id").isNumber())
                .andExpect(jsonPath("$[1].id").value(2))
                .andExpect(jsonPath("$[1].name").isString())
                .andExpect(jsonPath("$[1].name").value("company2"));

    }

    @Test
    void should_return_company_save_with_id_when_perform_post_given_a_company() throws Exception {
        //given
        Company company1 = buildCompany1();
        String company1Json = mapper.writeValueAsString(company1);

        //when
        mockMvc.perform(MockMvcRequestBuilders.post("/companies")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(company1Json))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.name").isString())
                .andExpect(jsonPath("$.name").value("company1"));
        Company companySaved = companyRepository.findById(1L);
        assertEquals(company1.getId(), companySaved.getId());
        assertEquals(company1.getName(), companySaved.getName());

    }

    @Test
    void should_update_company_in_repo_when_perform_put_by_id_given_company_in_repo_and_update_info() throws Exception {

        //given
        Company company1 = buildCompany1();
        companyRepository.addCompany(company1);
        Company toBeUpdateCompany1 = buildCompany1();
        toBeUpdateCompany1.setName("company10086");
        String company1Json = mapper.writeValueAsString(toBeUpdateCompany1);

        //when
        mockMvc.perform(MockMvcRequestBuilders.put("/companies/{id}", 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(company1Json))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.name").isString())
                .andExpect(jsonPath("$.name").value("company10086"));
        Company conpany1InRepo = companyRepository.findById(1L);
        assertEquals(toBeUpdateCompany1.getName(), conpany1InRepo.getName());
    }

    @Test
    void should_del_company_in_repo_when_perform_del_by_id_given_companies() throws Exception {
        //given
        Company company = buildCompany1();
        companyRepository.addCompany(company);

        //when
        mockMvc.perform(MockMvcRequestBuilders.delete("/companies/{id}", 1))
                .andExpect(status().isNoContent());

        //then
        assertThrows(NotFoundException.class, () -> companyRepository.findById(1L));
    }

    @Test
    void should_del_employees_in_this_company_in_repo_when_perform_del_by_id_given_companies() throws Exception {
        //given
        Company company = buildCompany1();
        companyRepository.addCompany(company);

        Employee employee = new Employee(1L,"lily",25,"Female",3500);
        EmployeeRepository employeeRepository = new EmployeeRepository();
        employeeRepository.insert(employee);
        //when
        mockMvc.perform(MockMvcRequestBuilders.delete("/companies/{id}", 1))
                .andExpect(status().isNoContent());

        //then
        assertThrows(NotFoundException.class, () -> employeeRepository.findById(1L));
    }

}

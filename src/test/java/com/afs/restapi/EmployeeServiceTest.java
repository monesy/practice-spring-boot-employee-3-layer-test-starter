package com.afs.restapi;

import com.afs.restapi.exception.AgeNotMatchSalaryException;
import com.afs.restapi.exception.AgeOutOfRangeException;
import com.afs.restapi.exception.NotFoundException;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.EmployeeRepository;
import com.afs.restapi.service.EmployeeService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.mockito.Mockito.*;

public class EmployeeServiceTest {
    EmployeeRepository employeeRepository = mock(EmployeeRepository.class);
    EmployeeService employeeService = new EmployeeService(employeeRepository);

    @Test
    void should_return_exception_when_insert_to_repository_given_employee_service_and_employee_with_age_15_and_66() {
        //given
        Employee employeeYoung = new Employee(1L, "DanielOu", 15, "Female", 1500);
        Employee employeeOld = new Employee(2L, "DanielOuOld", 66, "Female", 1500);

        //when then

        Assertions.assertThrows(AgeOutOfRangeException.class, () -> employeeService.insert(employeeYoung));
        Assertions.assertThrows(AgeOutOfRangeException.class, () -> employeeService.insert(employeeOld));
        verify(employeeRepository,times(0)).insert(Mockito.any());
    }

    @Test
    void should_return_exception_when_insert_to_repository_given_employee_service_and_employee_with_age35_and_salary10000() {
        Employee employeeLoser = new Employee(1L, "DanielOu", 35, "Female", 10000);

        Assertions.assertThrows(AgeNotMatchSalaryException.class,()->employeeService.insert(employeeLoser));
        verify(employeeRepository,times(0)).insert(Mockito.any());

    }

    @Test
    void should_return_employee_with_status_true_when_insert_employee_given_a_employee_match_rule() {
        Employee employee = new Employee(1L,"Zhaosi",38,"Male",30000);
        Employee employeeMock = new Employee(1L,"Zhaosi",38,"Male",30000);
        employeeMock.setStatus(true);
        when(employeeRepository.insert(any())).thenReturn(employeeMock);

        Employee employeeInserted = employeeService.insert(employee);

        Assertions.assertTrue(employeeInserted.isStatus());
        verify(employeeRepository).insert(argThat(employeeSave -> {
            Assertions.assertTrue(employeeSave.isStatus());
            return true;
        }));
    }

    @Test
    void should_make_employee_status_is_false_when_delete_employee_given_a_employee() {
        Employee employee = new Employee(1L,"Zhaosi",38,"Male",30000);
        Employee employeeMock = new Employee(1L,"Zhaosi",38,"Male",30000);
        employeeMock.setStatus(false);
        when(employeeRepository.findById(any())).thenReturn(employeeMock);

        employeeService.insert(employee);
        employeeService.delete(employee.getId());

        Assertions.assertFalse(employeeMock.isStatus());
        verify(employeeRepository,times(1)).findById(Mockito.any());

    }

    @Test
    void should_return_NOTFOUNDEXCEPTION_when_find_employee_delete_employee_given_a_employee() {
        Employee employee = new Employee(1L,"Zhaosi",38,"Male",30000);
        Employee employeeMock = new Employee(1L,"Zhaosi",38,"Male",30000);
        employeeMock.setStatus(false);
        when(employeeRepository.findById(any())).thenReturn(employeeMock);

        employeeService.insert(employee);
        employeeService.delete(employee.getId());

        Assertions.assertThrows(NotFoundException.class,()->employeeService.findById(employee.getId()));
    }

    @Test
    void should_return_Exception_when_update_employee_given_a_deleted_employee() {
        Employee employee = new Employee(1L,"Zhaosi",38,"Male",30000);
//        employee.setStatus(true);
        Employee employeeNow = new Employee(1L,"Zhaosi",38,"Male",30000);
        employeeNow.setStatus(true);
        when(employeeRepository.findById(any())).thenReturn(employeeNow);
        when(employeeRepository.update(any(),any())).thenReturn(employeeNow);

        employeeService.insert(employee);
        employeeService.delete(employee.getId());

        Assertions.assertThrows(NotFoundException.class,()->employeeService.update(employee.getId(),employee));
        verify(employeeRepository,times(0)).update(Mockito.any(),Mockito.any());
    }
}
